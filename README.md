## Stages of development:
### Phase I - Gehirn 
* Simple note-taking/blogging app
* Markdown support
* Web Rest API
* React Web App/React Native mobile app

### Phase II - Seele
* Applying ML/AI to the blog's data
* A microservice dedicated for the AI-ish stuff
* Using T-SNE and visualisation tools to create a contextual map of notes
* WebGL visualisation on the client side

### Phase III - Wille
* Doing some crazy shit with ML/AI
* Some contextual search/mapping/visualising 


# 
## Components: 

### Casper - Frontend
* React frontend app
* Single Page Application (SPA)
* Served through one of Balthasar's modules


### Balthasar - Backend
* Django backend service
* Exposes a web REST API
* Deployed on PythonAnywhere


### Melchior - ML/AI Microservice
* Dedicated to ML/AI specific functions
* Used by Balthasar to analize his data


#
## Coding standards 

- cammelCase
- nazwy klas z wielkiej litery
- parametry funkcji zaczynające się od p_ np. def funkcja(p_self,p_parametr1,p_parametr2):
- funkcje i metody z małej litery
- stałe wielkimi literami np int STAŁA