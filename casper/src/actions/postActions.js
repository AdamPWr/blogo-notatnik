export const setNewPosts = posts => {
    return {
        type: 'SET_NEW_POSTS',
        posts
    }
}