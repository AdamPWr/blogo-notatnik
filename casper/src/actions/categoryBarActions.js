export const loadCategoriesNames = categories => {
    return {
        type: 'LOAD_CATEGORIES_NAMES',
        categories
    }
}