import React, { Component } from "react";
import { Link } from "react-router-dom";

import axios from 'axios'

import { connect } from 'react-redux';

//Import actions 
import { loadCategoriesNames } from '../../actions/categoryBarActions';

const url = "https://wintermute.pythonanywhere.com";

class Header extends Component {

  constructor(props) {
    super(props);
    // this.state = {
    //   has_shadow: true
    // };
  }

  componentDidMount() {
    axios.get(url + "/api/tags")
      .then(res => {
        this.props.loadCategoriesNames(res.data);
        console.log(res.data)
      })
  }

  // componentDidMount() {
  //   //window.addEventListener('scroll', this.handleScroll)
  //   window.addEventListener("scroll", function(){

  //   this.setState({
  //     has_shadow: false
  //   })

  //     // window.scrollY > 0 ?
  //     //   this.setState({
  //     //     has_shadow: false
  //     //   }) : 
  //     //   this.setState({
  //     //     has_shadow: false
  //     //   })
  //   })
  // }

  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.handleScroll)
  // }

  handleScroll(event) {
    // if(window.scrollY < n.offsetTop + n.offsetHeight - 200) 
    //    { i.classList.remove("header--has-shadow";}
    // else
    //  { i.classList.add("header--has-shadow"));}
    window.scrollY > 0 ? console.log("t") : console.log("n");


  }

  // componentDidMount(){
  //   window.addEventListener("scroll", function(){
  //     window.scrollY < 0 ? console.log("t") : console.log("n")
  //   });
  // }

  render() {
    // window.addEventListener("scroll", function(){

    // })

    const categoriesList = this.props.categories.length ? (
      this.props.categories.map(category => {
        return (

          <li className="navigation__element--dropdown"
          key = {category.name}>
            <Link 
            className="navigation__link" 
            to={`/category/${category.slug}`}
            key = {category.slug}>
              {category.name}
            </Link>
          </li>


        )
      })
    ) : (
        <li className="navigation__element--dropdown">
          No categories!
        </li>
      );





    return (
      <header className="header">

        <nav className="navigation">
          <div className="navigation__element--logo">
            <Link className="navigation__link" to="/">
              Notatnik Programisty
              </Link>
          </div>
          <ul className="navigation__ul">

            <li className="navigation__element">
              <Link className="navigation__link" to="/about">
                O MNIE
              </Link>
            </li>
            <li className="navigation__element">
              <Link className="navigation__link" to="/contact">
                KONTAKT
              </Link>
            </li>
            <li className="navigation__element">
              <span className="navigation__link">KATEGORIE</span>
              <ul className="navigation__ul--dropdown">
                {categoriesList}
              </ul>
            </li>
          </ul>
        </nav>
      </header>
    )
  }

}
const mapStateToProps = state => {
  return {
    categories: state.categories.categories
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadCategoriesNames: categories => { dispatch(loadCategoriesNames(categories)) }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Header);
