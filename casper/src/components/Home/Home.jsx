import React, { Component } from 'react'
import axios from 'axios'

import Postcard from './Postcard'
import { connect } from 'react-redux';

//Import actions 
import { setNewPosts } from '../../actions/postActions';

const url = "https://wintermute.pythonanywhere.com";

class Home extends Component {


  componentDidMount() {
    axios.get(url + "/api/posts")
      .then(res => {
        this.props.setNewPosts(res.data);
        console.log(res.data)
      })
  }
  render() {
    //const postsL  = posts.posts
    const postList = this.props.posts.length ? (
      this.props.posts.map(post => {
        return (
          <Postcard
            id={post.id}
            title={post.title}
            date={post.date}
            img={`https://wintermute.pythonanywhere.com${post.image_url}`}
            tags={post.tags}
            slug={post.slug}
          />
        )
      })
    ) : (
        <div className="center">No posts to show</div>
      );

    return (
        <div className="home">
          {/* <h4 className="center">Home</h4> */}
          {postList}
        </div>
      // </div>
    )
  }
}


const mapStateToProps = state => {
  return {
    posts: state.posts.posts
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setNewPosts: posts => { dispatch(setNewPosts(posts)) }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Home);
