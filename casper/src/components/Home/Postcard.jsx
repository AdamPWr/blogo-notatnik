import React from 'react';
import PropTypes from 'prop-types'
import { Link, Route } from 'react-router-dom'

import Category from '../Category/Category'
import Post from '../Post/Post'



const Postcard = ({ id, title, date, img, tags, slug }) => {

    // const mappedTags = new Map(Object.entries(tags));

    const style = {
        backgroundImage: `url(${img})`
    };


    return (

        <div
            className="postcard"
            key={id}
            style={style}>

            <span className="postcard__title" key={id}>{title}</span>
            <span className="postcard__date" key={id}>{date}</span>
            <div className="postcard__tags_container" key={id}>
                {/* {tagsList} */}
                {
                    tags.map(tag => (

                        <Link to={`/category/${tag.slug}`}
                            key={tag.slug}
                            className="postcard__tag_link">
                            <button className="postcard__tag">
                                {tag.name}
                            </button>

                        </Link>

                    ))

                }
                <Route path="/category/:id" component={Category} />
                {/* <Route path="/category/:id" render={(props) => (<Category category_name="chuj" {...props}/>)} /> */}
            </div>
            <Link to={`/post/${slug}`} className="postcard__button_link">
                <button
                    className="postcard__button"
                    key={id}>
                    ZOBACZ POST
                    </button>
            </Link>


        </div>
    )
}

//TODO
// Postcard.PropTypes = {
//     id: PropTypes.string,
//     title: PropTypes.string,
//     date: PropTypes.string,
//     img: PropTypes.string
// }

export default Postcard;