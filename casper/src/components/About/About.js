import React from 'react';

const About = () => {
    return (
        <div className="about">
            <h1>
                Prosty CMS z obsługą Markdowna.
            </h1>
            <span>
                Normalnie powinny być tu informacje o autorze.
            </span>
        </div>
    )
}

export default About;