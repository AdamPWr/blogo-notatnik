import React, { Component } from 'react'
import axios from 'axios'
import Markdown from 'markdown-to-jsx';

class Post extends Component {
  state = {
    id: null,
    post_content: {}
  }

  componentDidMount(){
    let id = this.props.match.params.post_slug;
    this.setState({
        id: id
      })

    axios.get(`https://wintermute.pythonanywhere.com/api/posts/${id}/`)
        .then(res => {
          console.log(res);
          this.setState({
            post_content: res.data
          });
        })


    
  }
  render() {
    console.log(this.state.post_content);

    
    let postContent = Object.entries(this.state.post_content).length != 0 ?
      (
          
        <div className="post__container">
        
            <img 
                src={`https://wintermute.pythonanywhere.com${this.state.post_content.image_url}`} 
                className="post__img"/>
            <div className="post__content_container">
              <h1>{this.state.post_content.title}</h1>
              <div className="post__author_container">
                <img
                    src={`https://wintermute.pythonanywhere.com${this.state.post_content.author.avatar_url}`} 
                    className="post__avatar"/>
                <div className="post__author_name_container">
                  <span>{this.state.post_content.author.full_name}</span>
                  <span>{new Date(this.state.post_content.date_created).toLocaleDateString("pl")}</span>
                </div>
              </div>
              <div className="post__markdown">
                <Markdown>{this.state.post_content.content}</Markdown>
              </div>
              </div>        
        </div>
      ) : (
        <div className="center">No posts to show</div>
      );

    return (
        <div className="post">
        {postContent}
        </div>
    )
  }
}

export default Post