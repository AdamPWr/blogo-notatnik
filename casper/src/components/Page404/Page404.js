import React from 'react';
import Image404 from '../../assets/images/img404.png';
import { Link } from 'react-router-dom'


const Page404 = () => {
    return (
        <div className="page404">
            <div className="page404__container">
                <img src={Image404} alt="404" />
                <span className="page404__information_text">
                    Ojej, strona nie istnieje.
                </span>
                <Link to={`/`}
                    className="button_link">
                    <button className="page404__button">
                        WRÓC NA STRONĘ GŁÓWNĄ
                    </button>
                </Link>
            </div>
        </div>
    )
}

export default Page404;