import React, { Component } from 'react';
//import Navbar from './components/Navbar'
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom'
import Home from './components/Home/Home'
import About from './components/About/About'
import Category from './components/Category/Category'
import Contact from './components/Contact/Contact'
import Post from './components/Post/Post'
import Page404 from './components/Page404/Page404'

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers/rootReducer';

//Import styles
import './styles/index.scss';
import Header from './components/Header/Header';

const store = createStore(rootReducer);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <BrowserRouter basename={'/blogo-notatnik'}>
        <div className="app">
        <Header />
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/about' component={About} />
            <Route path='/contact' component={Contact} />
            <Route path="/category/:category_slug" component={Category} />
            <Route path='/post/:post_slug' component={Post} />
            {/* <Redirect to="/404" component={Home}/> */}
            <Route component={Page404} />
            
          </Switch>
        </div>
      </BrowserRouter>
      </Provider>
    );
  }
}

export default App;