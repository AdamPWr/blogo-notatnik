const initState = {
    categories: []
}

const categoryBarReducer = (state = initState, action) => {
    switch(action.type) {
        case 'LOAD_CATEGORIES_NAMES':
            return {
                ...state,
                categories: action.categories
            }
        default:
            return state;
    }
}

export default categoryBarReducer;