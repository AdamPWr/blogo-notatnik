import { combineReducers } from 'redux';
import postReducer from './postReducer';
import categoryBarReducer from './categoryBarReducer';


const rootReducer = combineReducers({
    posts: postReducer,
    categories: categoryBarReducer
});

export default rootReducer;