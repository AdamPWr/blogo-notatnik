const initState = {
    posts: []
}

const postReducer = (state = initState, action) => {
    switch(action.type) {
        case 'SET_NEW_POSTS':
            return {
                ...state,
                posts: action.posts
            }
        default:
            return state;
    }
}

export default postReducer;