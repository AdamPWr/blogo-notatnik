from blog import views
from django.urls import path, include
from rest_framework import routers

router = routers.DefaultRouter()
router.register('posts', views.PostViewSet)
router.register('authors', views.AuthorViewSet)
router.register('tags', views.TagViewSet)

urlpatterns = [
    path('', include(router.urls))
]