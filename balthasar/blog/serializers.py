from rest_framework import serializers
from .models import Post, Author, Tag


class AuthorSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Author
        fields = ('slug', 'url', 'full_name', 'avatar_url')

        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }



class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('url', 'name', 'slug')
    
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class PostsListSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()
    tags = TagSerializer(many=True)

    class Meta:
        model = Post
        fields = ( 'title', 'slug', 'url', 'image_url', 'author', 'tags', 'date_created')
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class PostSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()
    tags = TagSerializer(many=True)

    class Meta:
        model = Post
        fields = ('id', 'url', 'title', 'image_url', 'content', 'author', 'tags', 'date_created')
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }



class TagWithRelatedPostsSerializer(serializers.ModelSerializer):

    post_set = PostsListSerializer(many=True)

    class Meta:
        model = Tag
        fields = ('url', 'name', 'slug', 'post_set')
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
