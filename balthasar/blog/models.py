from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
# Create your models here.


class Tag(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)  # Call the "real" save() method.

    def __str__(self):
        return f'{self.name} - /{self.slug}/'


class Author(models.Model):
    user        = models.ForeignKey(User, on_delete=models.CASCADE)
    avatar      = models.ImageField(upload_to='avatars')
    full_name   = models.CharField(max_length=100, default='No name set')
    slug = models.SlugField(max_length=100, unique=True, blank=True)
    #permission

    def save(self, *args, **kwargs):
        self.slug = slugify(self.full_name)
        super().save(*args, **kwargs)  # Call the "real" save() method.

    def __str__(self):
        return f'{self.user.username}: {self.full_name}'

    @property
    def avatar_url(self):
        return self.avatar.url


class Post(models.Model):
    title           = models.CharField(max_length=100)
    content         = models.TextField()
    image           = models.ImageField(upload_to='post_images')
    date_created    = models.DateTimeField(auto_now=True)
    slug = models.SlugField(max_length=100, unique=True, blank=True)

    tags            = models.ManyToManyField(Tag)
    author          = models.ForeignKey(Author, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def image_url(self):
        return self.image.url

    def __str__(self):
        date = self.date_created
        return f"{self.title} - {date.year}.{'0' + str(date.month) if date.month < 10  else date.month}.{date.day} {date.hour}:{date.minute}"


