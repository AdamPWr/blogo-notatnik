from .models import Post, Author, Tag
from rest_framework import viewsets
from .serializers import PostSerializer, PostsListSerializer, AuthorSerializer, TagSerializer, TagWithRelatedPostsSerializer
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.request import Request

class PostViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Post.objects.all().order_by('-date_created')
    serializer_class = PostSerializer
    lookup_field = 'slug'

    # Lists are sent using the `PostsListsSerializer`, which only sends
    # the title and the URL of a post
    def list(self, request):
        context = {
            'request': request
        }

        queryset = Post.objects.all()
        lookup_field = 'slug'
        serializer = PostsListSerializer(queryset, many=True, context=context)
        return Response(serializer.data)


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    lookup_field = 'slug'


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagWithRelatedPostsSerializer
    lookup_field = 'slug'

    def list(self, request):
        context = {
            'request': request
        }

        queryset = Tag.objects.all()
        lookup_field = 'slug'
        serializer = TagSerializer(queryset, many=True, context=context)
        return Response(serializer.data)
