from django.apps import AppConfig


class PseudoCiConfig(AppConfig):
    name = 'pseudo_ci'
