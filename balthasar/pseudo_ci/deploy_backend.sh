# Aww yeah hardcoded paths
cd /home/wintermute/blogo-notatnik/balthasar


echo "# Pulling.."
git pull

echo "# Activating repo's virtual env.."
source /home/wintermute/blogo-notatnik/balthasar/venv/bin/activate

echo "# Installing new libraries.."
pip install -r /home/wintermute/blogo-notatnik/balthasar/requirements.txt

echo "# Migrating changes.."
python /home/wintermute/blogo-notatnik/balthasar/manage.py migrate --noinput

echo "# Restarting the app.."
touch /var/www/wintermute_pythonanywhere_com_wsgi.py
