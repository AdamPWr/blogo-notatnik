from django.shortcuts import render
from django.http import JsonResponse
import os
import subprocess
# Create your views here.



def trigger_ci(request):

    pseudo_ci_script = os.path.join(
        os.path.dirname(os.path.dirname(__file__)),
        'pseudo_ci',
        'deploy_backend.sh'
        )


    pseudo_ci_log = os.path.join(
        os.path.dirname(os.path.dirname(__file__)),
        'pseudo_ci',
        'last_deploy_log'
        )

    subprocess.Popen(['/bin/bash', pseudo_ci_script, '>', pseudo_ci_log])

    # subprocess.Popen(['/bin/sh', os.path.expanduser('~/code/python/test/sleeper.sh')])
    return JsonResponse({'status': 'starting..'})