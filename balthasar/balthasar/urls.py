"""balthasar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import blog.urls
import pseudo_ci.urls
from django.http import HttpResponse
from django.conf.urls.static import static
from balthasar import settings

from rest_framework.authtoken import views

def home_page(request):
    return HttpResponse('Let\'s all love Lain!')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(blog.urls)),
    path('ci/', include(pseudo_ci.urls)),
    path('', home_page),

    # path('rest-auth/', include('rest_auth.urls')),  # TODO: FORGOT WHAT THAT IS
    path('api-token-obtain/', views.obtain_auth_token)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)